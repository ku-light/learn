
```
docker tag quick-server:latest 1605009393/quick-server:latest
docker push 1605009393/quick-server:latest
docker pull 1605009393/quick-server:latest
docker run --name quick-server -d -p 8080:8080 quick-server:latest


FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/hai-server.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]



Dockerfile



	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8
		</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<maven-jar-plugin.version>3.1.1</maven-jar-plugin.version>
		<dockerfile-maven-version>1.4.8</dockerfile-maven-version>
		<docker.repository.name>hai-server</docker.repository.name>
	</properties>


        <finalName>${docker.repository.name}</finalName>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<!--指定版本号 -->
				<version>2.7.1</version>
				<configuration>
					<includeSystemScope>true</includeSystemScope>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>repackage</goal>
							<goal>build-info</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

		</plugins>
```
