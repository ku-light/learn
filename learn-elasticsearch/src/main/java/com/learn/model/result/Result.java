package com.learn.model.result;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private String code;
    private Object messages;
    private Object result;
    public static Result success(Object result) {
        return  Result.builder().code("1000000").messages("成功").result(result).build();
    }

    public static Result success() {
        return  Result.builder().code("1000000").messages("成功").result("").build();
    }

    public static Result fail(String messages) {
        return  Result.builder().code("4000000").messages(messages).result("").build();
    }

    public static Result err(String messages) {
        return  Result.builder().code("5000000").messages(messages).result("").build();
    }
}
