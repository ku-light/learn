package com.learn.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.learn.model.BookBean;
import com.learn.model.result.Result;
import com.learn.service.BookService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("book")
public class BookController {
    @Autowired
    private BookService bookService;

    @RequestMapping("findById/{id}")
    public Result getBookId(@PathVariable String id){
        Optional<BookBean> opt = bookService.findById(id);
        log.info("查詢單個==>{}",opt);
        BookBean book=opt.get();
        return Result.success(book);
    }

    @RequestMapping("/save")
    public Result bookSave(@RequestBody BookBean book){
       log.info("book====>{}",book);
        bookService.save(book);
        return Result.success();
    }

    @PostMapping("/all")
    public Result all(){
        Iterable<BookBean> result =  bookService.findAll();
        log.info("查詢全部==>{}",result);
        return Result.success(result);
    }
}
