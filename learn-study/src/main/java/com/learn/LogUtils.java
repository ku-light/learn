package com.learn;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

/**
 * @Description
 * @ClassName LogUtils
 * @Author t-wencheng.ma
 * @date 2020.07.14 18:11
 */
@Component
public class LogUtils {
    @PostConstruct
    public void init(){
        try {
            System.out.println("1111");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
