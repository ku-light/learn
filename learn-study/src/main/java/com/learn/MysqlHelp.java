package com.learn;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @Description
 * @ClassName MysqlHelp
 * @Author t-wencheng.ma
 * @date 2020.07.14 18:16
 */
@Component
public class MysqlHelp {

    @PostConstruct
    public  void intMysql(){
        System.out.println("初始化intMysql");
    }
}
